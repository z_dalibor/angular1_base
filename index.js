const express = require("express");
const livereload = require("livereload");
const path = require("path");

process.env.NODE_ENV = process.env.NODE_ENV || "development";

const publicDir = process.env.NODE_ENV === "production" ? "build/prod" : "build/dev";

const app = express();

app.use(express.static(publicDir));

app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});

// livereload
if (!(process.env.NODE_ENV === "production")) {
    const server = livereload.createServer({
        debug: false
    });
    server.watch(path.join(__dirname, publicDir));
}
