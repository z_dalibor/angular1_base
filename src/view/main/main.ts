import IModule = angular.IModule;
import IDirective = angular.IDirective;
import IComponentOptions = angular.IComponentOptions;
import {StateProvider} from "angular-ui-router";

class MainViewCtrl {}

function conf($stateProvider: StateProvider) {
    $stateProvider
        .state("app", {
            abstract: true,
            views: {
                "app": "mainView"
            }
        });
}

function register(module: IModule) {
    module.config(conf)
        .component("mainView", {
            template: require("./main.html"),
            controller: MainViewCtrl
        });
}

MainViewCtrl.$inject = [];
conf.$inject = ["$stateProvider"];

export default register;