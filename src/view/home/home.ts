import IModule = angular.IModule;
import IDirective = angular.IDirective;
import IComponentOptions = angular.IComponentOptions;
import {StateProvider} from "angular-ui-router";

class HomeViewCtrl {}

function conf($stateProvider: StateProvider) {
    $stateProvider
        .state("app.home", {
            url: "/",
            views: {
                "main": "homeView"
            }
        });
}

function register(module: IModule) {
    module
        .config(conf)
        .component("homeView", {
            template: require("./home.html"),
            controller: HomeViewCtrl
        });
}

HomeViewCtrl.$inject = [];
conf.$inject = ["$stateProvider"];

export default register;