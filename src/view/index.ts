import {module} from "angular";

import mainView from "./main/main";
import homeView from "./home/home";
import errorView from "./error/error";

const viewModule = "app.view";

let view = module(viewModule, []);
mainView(view);
homeView(view);
errorView(view);

export default viewModule;