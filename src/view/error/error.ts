import IModule = angular.IModule;
import IDirective = angular.IDirective;
import IComponentOptions = angular.IComponentOptions;
import {StateProvider, StateParams} from "angular-ui-router";

interface ErrorStateParams extends StateParams {
    id?: number;
}

class ErrorViewCtrl {}

function conf($stateProvider: StateProvider) {
    $stateProvider
        .state("app.errorUnknown", {
            url: "/error",
            views: {
                "main": "errorView"
            },
            resolve: {
                errorId: function (): number {
                    return 0;
                }
            }
        })
        .state("app.error", {
            url: "/error/{id:int}",
            views: {
                "main": "errorView"
            },
            resolve: {
                errorId: function ($stateParams: ErrorStateParams): number {
                    return $stateParams.id;
                }
            }
        });
}

function register(module: IModule) {
    module
        .config(conf)
        .component("errorView", {
            template: require("./error.html"),
            controller: ErrorViewCtrl,
            bindings: {
                errorId: "<"
            }
        });
}

ErrorViewCtrl.$inject = [];
conf.$inject = ["$stateProvider"];

export default register;