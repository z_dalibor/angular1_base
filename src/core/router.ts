import {UrlRouterProvider, TransitionService, Transition} from "angular-ui-router";
import {INgRedux} from "ng-redux";
import {AppAction} from "./redux";

export function conf($urlRouterProvider: UrlRouterProvider) {
    $urlRouterProvider
        .when("", "/")
        .otherwise("/error/404");
}

export const routerActionTypes = {
    ROUTER_STATE_CHANGE: "ROUTER_STATE_CHANGE"
};

export function routerStateChangeAction(routerState: any): AppAction {
    return {
        type: routerActionTypes.ROUTER_STATE_CHANGE,
        payload: routerState
    };
}

export function run($transitions: TransitionService, $ngRedux: INgRedux) {
    $transitions.onSuccess({}, (transition: Transition) => {
        let stateTo = transition.$to();
        let stateFrom = transition.$from();
        $ngRedux.dispatch(routerStateChangeAction({
            from: { name: stateFrom.name },
            to: { name: stateTo.name }
        }));
    });
}

conf.$inject = ["$urlRouterProvider"];
run.$inject = ["$transitions", "$ngRedux"];