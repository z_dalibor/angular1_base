import ITranslateProvider = angular.translate.ITranslateProvider;
import {INgRedux} from "ng-redux";
import IRootScopeService = angular.IRootScopeService;
import {AppAction} from "./redux";

export function conf($translateProvider: ITranslateProvider) {
    $translateProvider
        .registerAvailableLanguageKeys(["en", "sr"], {
            "en_*": "en",
            "sr_*": "sr"
        })
        .useStaticFilesLoader({
            prefix: "locale/",
            suffix: ".json"
        })
        .determinePreferredLanguage()
        .fallbackLanguage("en")
        .useCookieStorage();
}

export const languageActionTypes = {
    LANGUAGE_CHANGE: "LANGUAGE_CHANGE"
};

export function languageChangeAction(language: any): AppAction {
    return {
        type: languageActionTypes.LANGUAGE_CHANGE,
        payload: language
    };
}

export function run($rootScope: IRootScopeService, $ngRedux: INgRedux) {
    $rootScope.$on("$translateChangeSuccess", (event: any, language: any) => {
        $ngRedux.dispatch(languageChangeAction(language));
    });
}

conf.$inject = ["$translateProvider"];
run.$inject = ["$rootScope", "$ngRedux"];
