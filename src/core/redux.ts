import {INgReduxProvider} from "ng-redux";
import {Action} from "redux";
import {routerActionTypes} from "./router";
import {languageActionTypes} from "./translate";
import * as _ from "lodash";
import reduxThunk from "redux-thunk";
import * as reduxLogger from "redux-logger";

export interface AppAction extends Action {
    payload: any;
}

export function rootReducer(state: any = {}, action: AppAction): any {
    switch (action.type) {
        case routerActionTypes.ROUTER_STATE_CHANGE:
            return _.assign({}, state, {"router": action.payload});
        case languageActionTypes.LANGUAGE_CHANGE:
            return _.assign({}, state, {"language": action.payload});
        default:
            return state;
    }
}

export function conf($ngReduxProvider: INgReduxProvider) {
    let reducer = rootReducer;
    const logger = reduxLogger();
    $ngReduxProvider.createStoreWith(reducer, [
        reduxThunk, logger
    ]);
}

conf.$inject = ["$ngReduxProvider"];
