import "angular";
import "angular-animate";
import "angular-aria";
import "angular-messages";
import "angular-material";
import "angular-cookies";
import "angular-ui-router";

// translation service
import "angular-translate";
import "angular-translate-storage-cookie";
import "angular-translate-loader-static-files";

// redux
import "redux";
import "redux-thunk";
import "redux-logger";
import "ng-redux";

// other libraries
import "lodash";