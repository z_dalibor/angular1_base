import {module} from "angular";
import * as ngCookies from "angular-cookies";
import uiRouter from "angular-ui-router";
import ngRedux from "ng-redux";

import appView from "./view";

import * as translate from "./core/translate";
import * as router from "./core/router";
import * as redux from "./core/redux";

const appModule = "app";

let app = module(appModule, [
    "pascalprecht.translate", ngCookies, uiRouter,
    ngRedux, appView
]);

app
    .config(translate.conf)
    .config(router.conf)
    .config(redux.conf);

app
    .run(router.run)
    .run(translate.run);

export default appModule;