const webpack = require("webpack");
const path = require("path");

const wpProd = {
    output: {
        filename: "js/[name].bundle.min.js",
        chunkFilename: "js/[id].bundle.min.js"
    },
    devtool: "hidden-source-map",
    plugins: [
        new webpack.optimize.UglifyJsPlugin()
    ]
};

module.exports = wpProd;