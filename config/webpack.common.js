const webpack = require("webpack");
const path = require("path");

const ROOT = path.join(__dirname, "..");

const wpCommon = {
    context: path.join(ROOT, "src"),
    entry: {
        app: "./app.ts",
        vendors: "./vendors.ts"
    },
    output: {
        filename: "js/[name].bundle.js",
        chunkFilename: "js/[id].bundle.js"
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },
    module: {
        preLoaders: [
            {
                test: /\.tsx?$/,
                loader: "tslint-loader"
            }
        ],
        loaders: [
            {
                test: /\.tsx?$/,
                loaders: ["ng-annotate", "ts-loader"]
            },
            {
                test: /\.html$/,
                loader: "ng-cache?-url&module=app&prefix=[dir]/[dir]"
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendors']
        }),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development")
            }
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
    ]
};

module.exports = wpCommon;