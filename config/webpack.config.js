const merge = require("webpack-merge");

const wpCommon = require("./webpack.common");
const wpDev = require("./webpack.dev");
const wpProd = require("./webpack.prod");

const ENV = process.env.NODE_ENV || "development";

let config = wpCommon;
if (ENV === "production") {
    config = merge(config, wpProd);
} else {
    config = merge(config, wpDev);
}

module.exports = config;

