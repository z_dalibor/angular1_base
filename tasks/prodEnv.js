const env = require("gulp-env");

function prodEnvTask(cb) {
    env.set({
        NODE_ENV: 'production'
    });
    cb();
}

module.exports = prodEnvTask;