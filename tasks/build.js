const gulp = require("gulp");
const webpack = require("webpack-stream");
const plumber = require("gulp-plumber");

function buildTask() {
    const ENV = process.env.NODE_ENV || "development";
    const DEST = (ENV === "production") ? "build/prod" : "build/dev";

    let wpConfig = require('../config/webpack.config');
    if (process.env.WATCH)
        wpConfig.watch = true;

    return gulp.src('src/app.ts')
        .pipe(plumber())
        .pipe(webpack(wpConfig))
        .pipe(gulp.dest(DEST));
}

module.exports = buildTask;