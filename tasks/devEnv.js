const env = require("gulp-env");

function devEnvTask(cb) {
    env.set({
        NODE_ENV: 'development'
    });
    cb();
}

module.exports = devEnvTask;