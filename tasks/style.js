const gulp = require("gulp");
const concat = require("gulp-concat");
const sass = require("gulp-sass");
const merge = require("merge-stream");
const sourcemaps = require("gulp-sourcemaps");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const precss = require("precss");
const cssnano = require("cssnano");
const reporter = require("postcss-reporter");
const rename = require("gulp-rename");
const gulpif = require("gulp-if");
const postcssCopy = require("postcss-copy");
const path = require("path");

function styleTask() {
    const ENV = process.env.NODE_ENV || "development";
    const DEST = (ENV === "production") ? "build/prod" : "build/dev";

    let vendors = gulp.src([
        require.resolve("material-design-icons/iconfont/material-icons.css"),
        require.resolve("angular-material/angular-material.css")
    ])
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat("vendors.css"))
        .pipe(sourcemaps.write());

    let main = gulp.src("web/scss/[^_]*.scss")
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass().on("error", sass.logError))
        .pipe(sourcemaps.write());

    function template(fileMeta) {
        let prefix = "";
        if (/gif|png|jpg|jpeg/i.test(fileMeta.ext))
            prefix = "img/";
        if (/eot|svg|ttf|woff|woff2/i.test(fileMeta.ext))
            prefix = "fonts/";
        return prefix + fileMeta.name +
            "_" + fileMeta.hash + "." + fileMeta.ext +
            fileMeta.query;
    }

    function relativePath(dirname, fileMeta, result, opts) {
        return path.join(opts.dest, "css");
    }

    return merge(vendors, main)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(postcss([
            postcssCopy({
                src: [
                    "web",
                    "node_modules"
                ],
                dest: DEST,
                template,
                relativePath
            }),
            autoprefixer(),
            precss(),
            cssnano({
                core: ENV === "production",
                discardUnused: {
                    fontFace: false
                }
            }),
            reporter({
                plugins: ["!postcss-discard-empty"]
            })
        ]))
        .pipe(gulpif(ENV === "production", rename({
            extname: ".min.css"
        })))
        .pipe(sourcemaps.write(".", {
            addComment: !(ENV === "production")
        }))
        .pipe(gulp.dest(path.join(DEST, "css")));
}

module.exports = styleTask;