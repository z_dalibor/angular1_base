const gulp = require("gulp");
const template = require("gulp-template");
const rename = require("gulp-rename");
const merge = require("merge-stream");

function templateTask() {
    const ENV = process.env.NODE_ENV || "development";
    const DEST = ENV === "production" ? "build/prod" : "build/dev";

    let cp1 = gulp.src([
        "web/favicon.ico"
    ], {base: "web"})
        .pipe(gulp.dest(DEST));

    let cp2 = gulp.src([
        "src/locale/*.json"
    ], {base: "src"})
        .pipe(gulp.dest(DEST));

    let tt = gulp.src("web/index.ejs")
        .pipe(template({
            ENV: ENV
        }))
        .pipe(rename({extname: ".html"}))
        .pipe(gulp.dest(DEST));

    return merge(tt, cp1, cp2);
}

module.exports = templateTask;