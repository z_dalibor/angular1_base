const gulp = require("gulp");
const env = require("gulp-env");
const gutil = require("gulp-util");

const devEnvTask = require("./tasks/devEnv");
const prodEnvTask = require("./tasks/prodEnv");
const buildTask = require("./tasks/build");
const styleTask = require("./tasks/style");
const templateTask = require("./tasks/template");

gulp.task("env:dev", devEnvTask);
gulp.task("env:prod", prodEnvTask);

gulp.task("build:dev", ["env:dev"], buildTask);
gulp.task("build:prod", ["env:prod"], buildTask);
gulp.task("build:watch", ["env:dev"], () => {
    env.set({WATCH: 1});
    return buildTask();
});

gulp.task("style:dev", ["env:dev"], styleTask);
gulp.task("style:prod", ["env:prod"], styleTask);
gulp.task("style:watch", ["style:dev"], () => {
    gutil.log("gulp watching for changes");
    gulp.watch("web/scss/**/*.scss", ["style:dev"]);
});

gulp.task("template:dev", ["env:dev"], templateTask);
gulp.task("template:prod", ["env:prod"], templateTask);